// @flow

import React from 'react';
import {
  Text, View, Button, TextInput,
  AsyncStorage,
  ScrollView, RefreshControl,
  KeyboardAvoidingView,
  ActivityIndicator
} from 'react-native';

import { connect, Provider } from 'react-redux';

import { Font } from 'expo';

import moment from 'moment';
import momentDurationFormatSetup from 'moment-duration-format';

import store from './store';
/*::
import type UserType from './store';
import type OrderedCategoryType from './store';
import type CategoryType from './store';
import type ExtendedErrorType from './store';
*/


// Utilities and constants
momentDurationFormatSetup(moment);
const timeformatter = dt => moment(dt).format("HH:mm");
const indices = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];


// Component classes

const ErrorNotice = connect(
  state => ({
    error: state.error
  })
)(class _ErrorNotice extends React.Component
  /*::<{ error: ExtendedErrorType }>*/
{
  render()
  {
    const { error } = this.props;
    const errorStyle = {
      backgroundColor: '#fff',
      color: '#c00',
      margin: 12,
      padding: 6
    };
    if (error)
    {
      return <View><Text style={errorStyle}>{error}</Text></View>;
    }
    return null;
  }
});

const StatusDisplay = connect(
  state => ({
    lastitem: state.lastitem,
    currenttime: state.currenttime,
    noitemloaded: state.noitemloaded,
    fontsloaded: state.fontsloaded
  })
)(class _StatusDisplay extends React.Component
  /*::<{
    lastitem: CategoryType,
    currenttime: Date,
    noitemloaded: boolean,
    fontsloaded: boolean
  }>*/
{
  render()
  {
    let { lastitem, noitemloaded } = this.props;
    const currenttime = this.props.currenttime;
    let categorytext = "", elapsedtext = "", timetext = timeformatter(currenttime);

    if (noitemloaded)
    {
      categorytext = "Loading...";
    }
    else if (lastitem)
    {
      const lastitemdate = new Date(lastitem.time_started);
      let lastdeltaseconds = (currenttime - lastitemdate) / 1000;
      let lastdelta = moment.duration(
        lastdeltaseconds,
        'seconds'
      ).format('hh:mm');

      if (lastdeltaseconds < 3600)
      {
        lastdelta = "00:" + lastdelta;
      }

      // idle time is shown negative
      if (lastitem.isIdle)
      {
        lastdelta = '-' + lastdelta;
      }
      categorytext = lastitem.isIdle ? "" : lastitem.categories.join(' ');
      elapsedtext = lastdelta;
    }

    const fontStyle = {
      fontFamily: this.props.fontsloaded ? 'major-mono-display-regular' : 'monospace',
      fontSize: 18,
      textTransform: 'lowercase'
    };

    const statusDisplayStyle = {
      backgroundColor: '#7bb5ce',
      color: '#1a2428'
    };

    const currentTaskCategoriesStyle = {
      ...fontStyle,
      margin: 8,
      marginTop: 4,
      marginBottom: 4,
      height: 25
    };

    const statusTimesStyle = {
      margin: 8,
      marginTop: 4,
      flexDirection: 'row',
      gridGap: 5,
      height: 25
    };

    const currentTaskElapsedStyle = {
      ...fontStyle,
      flex: 1,
      textAlign: 'left'
    };

    const currentTimeStyle = {
      ...fontStyle,
      flex: 1,
      textAlign: 'right'
    };

    return (
      <View className="status-display" style={statusDisplayStyle}>
        <Text
          className="current-task-categories"
          style={currentTaskCategoriesStyle}
        >
          { categorytext.toLowerCase() }
        </Text>
        <View className="status-times" style={statusTimesStyle}>
          <Text className="current-task-elapsed" style={currentTaskElapsedStyle}>
            { elapsedtext }
          </Text>
          <Text className="current-time" style={currentTimeStyle}>{ timetext }</Text>
        </View>
      </View>
    );
  }
});

const CategoryButtons = connect(
  state => ({
    usercategories: state.usercategories.filter(c => !!c).map(c => c.category),
    userloading: state.userloading,
    selectedcategories: state.selectedcategories || new Set()
  })
)(class _CategoryButtons extends React.Component
  /*::<{
    usercategories: OrderedCategoryType,
    userloading: boolean,
    selectedcategories: Set<String>,
    dispatch: Function
  }>*/
{
  onPress(category, evt)
  {
    this.props.dispatch({ type: 'TOGGLE_SELECTED_CATEGORY', category });
  }

  render()
  {
    const { usercategories, userloading, selectedcategories } = this.props;

    const categoryButtonContainerStyle = {
      flexDirection: 'column',
      marginTop: 15
    };

    const categoryButtonRowStyle = {
      display: 'flex',
      flexDirection: 'row',
      marginBottom: 15
    };

    const categoryButtonLoaderStyle = {
      flex: 1,
      justifyContent: 'center',
      backgroundColor: '#0b4f6c',
      color: '#d9e1f4',
      alignSelf: 'stretch'
    };
    
    const categoryButtonStyle = {
      ...categoryButtonLoaderStyle,
    };

    return (
      <View style={categoryButtonContainerStyle}>
        {
          userloading ?
            <ActivityIndicator /> : null
        }
        <View className="category-button-container" style={categoryButtonRowStyle}>
          {
            !userloading ? usercategories.slice(0, 5).map(
              (category, i) => {
                const isselected = selectedcategories.has(category.name);
                return (
                  <View key={i} style={{ flex: 1, marginLeft: 5, marginRight: 5 }}>
                    <Button
                      className={"category-button"}
                      style={categoryButtonStyle}
                      color={isselected ? 'green' : '#0b4f6c'}
                      onPress={this.onPress.bind(this, category.name)}
                      title={category.name}
                    />
                  </View>
                )
              }
            )
            : null
          }
        </View>
        <View className="category-button-container" style={categoryButtonRowStyle}>
          {
            !userloading ?
             usercategories.slice(5, 10).map(
              (category, i) => {
                const isselected = selectedcategories.has(category.name);
                return (
                  <View key={i + 5} style={{ flex: 1, marginLeft: 5, marginRight: 5 }}>
                    <Button
                      className={"category-button"}
                      style={categoryButtonStyle}
                      color={isselected ? 'green' : '#0b4f6c'}
                      onPress={this.onPress.bind(this, category.name)}
                      title={category.name}
                    />
                  </View>
                )
              }
            )
            : null
          }
        </View>
      </View>
    );
  }
});

const ActionButton = connect(
  state => ({
    lastitem: state.lastitem,
    selectedcategories: state.selectedcategories || new Set(),
    fontsloaded: state.fontsloaded
  })
)(class _ActionButton extends React.Component
  /*::<{
    lastitem: CategoryType,
    selectedcategories: Set<String>,
    fontsloaded: boolean,
    dispatch: Function
  }>*/
{
  onPress(evt)
  {
    let categories = [];
    if (this.props.selectedcategories.size > 0)
    {
      categories = Array.from(this.props.selectedcategories);
    }
    else if (!this.props.lastitem.isIdle)
    {
      categories = ['END'];
    }
    if (categories.length)
    {
      this.props.dispatch({ type: 'CHANGE_TASK', categories });      
    }
  }

  render()
  {
    const hasselection = this.props.selectedcategories.size > 0;
    const enabled = !this.props.lastitem.isIdle || hasselection;
    const gc = this.props.lastitem.isIdle || hasselection;
    // console.log('action button render, gc=', gc);
    const text = enabled ? (gc ? 'GO' : 'STOP') : ' ';
    
    const actionButtonContainerStyle = {
      marginTop: 15
    };

    const actionButtonStyle = {
      fontFamily: this.props.fontsloaded ? 'vt323-regular' : 'monospace',
      fontSize: 24,
      height: 30,
      backgroundColor: '#0b4f6c',
      color: '#d9e1f4'
    };

    const cn = enabled ? (gc ? 'accept' : 'cancel') : 'standby';
    const cncolors = {
      'accept': 'green',
      'cancel': 'darkred',
      'standby': '#d9e1f4'
    }

    return (
      <View className="action-button-container" style={actionButtonContainerStyle}>
        <Button
          className={"action-button"}
          style={actionButtonStyle}
          color={cncolors[cn]}
          onPress={this.onPress.bind(this)}
          disabled={!enabled}
          title={text}
        />
      </View>
    );
  }
});

const CategoriesEditor = connect(
  state => ({
    usercategories: state.usercategories,
    editoropen: state.editoropen
  })
)(class _CategoriesEditor extends React.Component
  /*::<{
    usercategories: OrderedCategoryType,
    editoropen: boolean,
    dispatch: Function
  }, {
    names: Array<string>
  }>*/
{
  constructor(props)
  {
    super(props);
    this.state = { names: indices.map(i => '') };
  }

  toggle(evt)
  {
    this.props.dispatch({ type: 'TOGGLE_EDITOR' });
  }

  save(evt)
  {
    const categories = this.state.names.map(c => c.trim()).filter(c => !!c);
    this.props.dispatch({ type: 'SET_CATEGORIES', categories });
  }

  static getDerivedStateFromProps(props, state)
  {
    const names = indices.map(i => {
      let cat = props.usercategories[i] || { category: { name: '' } };
      return cat.category.name;
    });
    return { names };
  }
  
  handleInput(idx, name)
  {
    this.props.dispatch({ type: 'EDIT_CATEGORY', idx, name });
  }

  render()
  {
    const { editoropen } = this.props;

    // TODO: remove code duplication
    const tInputStyle = {
      marginBottom: 8,
      padding: 12,
      backgroundColor: '#0b4f6c'
    };

    return (
      <View className="categories-editor">
        <Button
          className="categories-editor-toggle"
          onPress={this.toggle.bind(this)}
          title="Edit..."
        />
        {editoropen ?
          <View className={"category-inputs-container" + (editoropen ? ' open' : '')}>
            {this.state.names.map(
              (c, i) => <TextInput
                name={i}
                key={i}
                type="text"
                value={c}
                onChangeText={this.handleInput.bind(this, i)}
                style={tInputStyle}
              />
            )}
            <Button className="categories-editor-save" onPress={this.save.bind(this)} title="Save" />
          </View>
          : null
        }
      </View>
    );
  }
});

const UserInfo = connect(
  state => ({
    user: state.user,
    fontsloaded: state.fontsloaded
  })
)(class _UserInfo extends React.Component
  /*::<{ user: UserType, dispatch: Function, fontsloaded: boolean }>*/
{
  onLogout()
  {
    this.props.dispatch({ type: 'LOGOUT' });
  }

  render()
  {
    const { user } = this.props;

    const userInfoStyle = {
      marginTop: 24
    };

    const userInfoTextStyle = {
      color: '#525E63',
      fontFamily: this.props.fontsloaded ? 'major-mono-display-regular' : 'monospace',
      fontSize: 12,
      textTransform: 'lowercase',
      textAlign: 'right'
    };

    return (
      <View className="user-info" style={userInfoStyle}>
        <Text style={userInfoTextStyle}>Logged in as { user.email }</Text>
        <Button title="Logout" onPress={this.onLogout.bind(this)} />
      </View>
    );
  }
});

const TitleText = connect(
  state => ({
    fontsloaded: state.fontsloaded
  })
)(class _TitleText extends React.Component
  /*::<{ fontsloaded: boolean }>*/
{
  render()
  {
    const titleStyle = {
      fontFamily: this.props.fontsloaded ? 'major-mono-display-regular' : 'monospace',
      fontSize: 30,
      marginTop: 30,
      marginBottom: 15,
      textAlign: 'center',
      color: '#d9e1f4'
    };
    return <Text className="title" style={titleStyle}>THE TASK SWITCH</Text>;
  }
}) 

class TimeTracker extends React.Component
/*::<{}>*/
{
  render()
  {
    return (
      <View>
        <StatusDisplay />
        <ActionButton />
        <CategoryButtons />
        <CategoriesEditor />
        <UserInfo />
      </View>
    );
  }
}

const LoginForm = connect(
)(class _LoginForm extends React.Component
  /*::<{ dispatch: Function }, { email: string, password: string }>*/
{
  constructor()
  {
    super();
    this.state = {
      email: '',
      password: ''
    };
  }

  setEmail(email)
  {
    this.setState({ email });
  }

  setPassword(password)
  {
    this.setState({ password });
  }

  doLogin()
  {
    const { email, password } = this.state;
    this.props.dispatch({ type: 'LOGIN', email, password });
  }

  render()
  {
    const tInputStyle = {
      marginBottom: 8,
      padding: 12,
      backgroundColor: '#0b4f6c',
      color: '#D9E1F4'
    };

    const loginButtonStyle = {
    };

    return (
      <View>
        <TextInput
          style={tInputStyle}
          onChangeText={this.setEmail.bind(this)}
          textContentType="emailAddress"
          keyboardType="email-address"
          placeholder="Email"
        />
        <TextInput
          style={tInputStyle}
          onChangeText={this.setPassword.bind(this)}
          textContentType="password"
          secureTextEntry={true}
          placeholder="Password"
        />
        <Button style={loginButtonStyle} title="Log In" onPress={this.doLogin.bind(this)} />
      </View>
    );
  }
});

const ConnectedApp = connect(
  state => ({
    token: state.token,
    refreshing: state.userloading || state.itemloading
  })
)(class _ConnectedApp extends React.Component
  /*::<{token: ?string, refreshing: boolean, dispatch: Function}>*/
{
  componentDidMount()
  {
    Font.loadAsync({
      'major-mono-display-regular': require('../assets/MajorMonoDisplay-Regular.ttf'),
      'vt323-regular': require('../assets/VT323-Regular.ttf')
    }).then(
      () => this.props.dispatch({ type: 'FONTS_LOADED' })
    );
  }

  onRefresh()
  {
    this.props.dispatch({ type: 'FETCH_USER' });
    this.props.dispatch({ type: 'FETCH_LAST_ITEM' });
  }

  render()
  {
    const appStyle = {
      backgroundColor: '#1A2428',
      flex: 1,
      flexDirection: 'column',
      justifyContent: 'center'
    };
    
    return (
      <KeyboardAvoidingView
        style={appStyle}
        behavior="padding"
        enabled
      >
      <ScrollView
        className="the-task-switch"
        style={{ margin: 18 }}
        refreshControl={
          <RefreshControl
            refreshing={this.props.refreshing}
            onRefresh={this.onRefresh.bind(this)}
          />
        }
      >
        <TitleText />
        {this.props.token ? 
          <TimeTracker /> :
          <LoginForm />
        }
        <ErrorNotice />
      </ScrollView>
      </KeyboardAvoidingView>
    );
  }
});



// Main app component

export default class PApp extends React.Component
/*::<void>*/
{
  render()
  {
    return (
      <Provider store={store}>
        <ConnectedApp />
      </Provider>
    );
  }
};

// Bootstrap - Go !
(function(){
  // check the async store for a token;
  // if we have one, set it in the store and tsclient
  // else the login form will be displayed anyway
  AsyncStorage.getItem('tts:token').then(
    token => store.dispatch({ type: 'SET_TOKEN' , token })
  );
}());

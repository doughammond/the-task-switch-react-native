// @flow

/*::
type ConfigType = {
  host: string,
  port: number,
  path: string,
  secure: boolean
};
*/

let cfg /*: ConfigType */ = {};


if (process.env.NODE_ENV === 'development') {
  // when running locally under dev server
  cfg = {
    "host": "192.168.0.19",
    "port": 5000,
    "path": "api/v1",
    "secure": false
  };
}

if (process.env.NODE_ENV === 'production') {
  // when running fully in production
  cfg = {
    "host": "tts.hamsterfight.co.uk",
    "port": 443,
    "path": "api/v1",
    "secure": true,
  };
}


export const config = cfg;

// @flow

import moment from 'moment';
import Promise from 'bluebird';

import { config } from './config/service';


// URL constants
const proto = config.secure ? 'https' : 'http';
const baseurl = `${proto}://${config.host}:${config.port}/`;
const apiurl = `${baseurl}${config.path}/`;


// Error classes
class APIError extends Error {
  /*::
  status: number;
  */

  constructor(message, status) {
    super(message);
    this.status = status;
  }
}

class LoginError extends Error {}


// Utility functions
const datetimeformatter = (dt) => moment(dt).format('YYYY-MM-DD HH:mm');


// REST API call
function api_handler(tokenprm /*: Promise<?string> */, resource /*: string */, method /*: 'get' | 'put' | 'post' */, args={}) {
  return tokenprm.then(
    token => fetch(apiurl + resource, {
      method,
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'Authentication-Token': token
      },
      body: (method == 'get' || method == 'head') ? null : JSON.stringify(args)
    })
  ).then(
    response => {
      if (!response.ok) {
        throw new APIError(`API Error: ${response.status}`, response.status);
      }
      return response.json();
    }
  ).catch(
    error => {
      throw new APIError(`Network error: ${error.message}`, -1);
    }
  );
}


// API client class
export class TimesheetsClient {
  /*::
  token: ?string;
  */

  constructor() {
    this.setToken(null);
  }

  login(email /*: String */, password /*: String */, remember /*: boolean */) {
    // flask-security/flask-wtf does not yet support CSRF with REST,
    // instead we have to get the HTML form and parse out the CSRF token
    // to use in the POST request

    // first issue a logout so that the fetch cookies are reset
    return fetch(baseurl + 'logout', {
      credentials: 'include'
    }).then(
      // now fetch the login form for CSRF token
      () => fetch(baseurl + 'login', {
        credentials: 'include'
      })
    ).then(
      response => {
        if (!response.ok) {
          throw new LoginError("Cannot initiate login sequence");
        }
        return response.text();
      }
    ).then(
      html => html.match(/<input id="csrf_token" name="csrf_token" type="hidden" value="(.*)">/)
    ).then(
      match => (match && match.length == 2) ? match[1] : null
    ).then(csrf_token => {
      if (!csrf_token) {
        throw new LoginError("Could not enforce login security");
      }
      // now that we have a CSRF token, we can perform the actual login
      return fetch(baseurl + 'login', {
        method: 'post',
        // need the cookie here for the CSRF token to work
        credentials: 'include',
        // disable redirect not implemented in RN
        //redirect: 'manual',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          email,
          password,
          remember: remember ? 'y' : null,
          csrf_token,
          // ideally this gives us the token back on login redirect,
          // but that is not working
          // next: `/${config.path}/token`
        })
      });
    }).then(
      response => {
        // we cannot check the response here;
        // - we are forced to follow the redirect and we have no visibility on the redirect response
        // - the redirect probably goes to / since the next parameter is not implemented
        // - the response code for URL / is meaningless depending on what may or may not be there,
        //   unrelated to the login flow
        if (response.status != 404 && response.status != 200) {
          throw new LoginError("Could not validate credentials (2)");
        }
        return fetch(apiurl + 'token', {
          // we need the cookie here to use the authenticated
          // session from the previous step.
          credentials: 'include'
        })
      }).then(
      response => {
        // complain if we did not get a token
        const wasredirected = response.url.startsWith(baseurl + 'login');
        if (!response.ok || wasredirected) {
          throw new LoginError("Could not validate credentials (1)");
        }
        return response.text();
      }
    ).then(
      // finally if all the above worked, we have a token to use
      // for further REST API calls
      token => this.setToken(token)
    );
  }

  logout() {
    return fetch(baseurl + 'logout', {
      credentials: 'include'
    }).then(
      () => this.setToken(null)
    );
  }

  setToken(token /*: ?string */) {
    this.token = Promise.resolve(token);
    return this.token;
  }

  getUser() {
    return api_handler(this.token, 'user', 'get');
  }

  getLastItem() {
    return api_handler(this.token, 'timesheet_item/latest', 'get');
  }

  putTimesheetItem(categories /*: Array<String> */) {
    return api_handler(this.token, 'timesheet_item', 'put', {
      time_started: datetimeformatter(new Date()),
      categories
    });
  }

  putUserCategories(categories /*: Array<String> */) {
    return api_handler(this.token, 'user/categories', 'put', {
      categories
    });
  }
};

export const Client = new TimesheetsClient();

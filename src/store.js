// @flow

import { AsyncStorage } from 'react-native';

import { createStore, compose, applyMiddleware } from 'redux';

import bluebird from 'bluebird';

import Reactotron from '../ReactotronConfig';
import { Client as tsclient } from './client';

if (__DEV__) {
    Reactotron.connect();
}

/*::
type TimesheetItemType = {
  isIdle: boolean,
  categories: Array<String>,
  time_started: Date
};

type CategoryType = {
  name: String
};

type OrderedCategoriesType = Array<{
  order: number,
  category: CategoryType
}>;

type AppStateType = {
  currenttime: Date,
  lastitem: ?TimesheetItemType,
  user: ?Object,
  usercategories: OrderedCategoriesType,
  userloading: boolean,
  itemloading: boolean,
  selectedcategories: Set<String>,
  noitemloaded: boolean,
  editoropen: boolean,
  saving: boolean,
  error: ?Error,
  fontsloaded: boolean,
  token: ?string,
  timer_interval: ?IntervalID,
  fetch_interval: ?IntervalID
};

type UserType = {
  categories: OrderedCategoriesType
};

type ExtendedErrorType = Error & {
  status: number
};

type action_FONTS_LOADED = { type: 'FONTS_LOADED' };
type action_FETCH_USER = { type: 'FETCH_USER', saving?: boolean };
type action_FETCH_USER_SUCCESS = { type: 'FETCH_USER_SUCCESS', user: UserType };
type action_API_ERROR = { type: 'API_ERROR', error: ExtendedErrorType, call: String };
type action_CLEAR_ERROR = { type: 'CLEAR_ERROR' };
type action_SET_TIME = { type: 'SET_TIME', time: Date };
type action_FETCH_LAST_ITEM = { type: 'FETCH_LAST_ITEM' };
type action_FETCH_LAST_ITEM_SUCCESS = { type: 'FETCH_LAST_ITEM_SUCCESS', lastitem: TimesheetItemType };
type action_TOGGLE_SELECTED_CATEGORY = { type: 'TOGGLE_SELECTED_CATEGORY', category: String };
type action_CLEAR_SELECTED_CATEGORIES = { type: 'CLEAR_SELECTED_CATEGORIES' };
type action_CHANGE_TASK = { type: 'CHANGE_TASK', categories: Array<String> };
type action_TOGGLE_EDITOR = { type: 'TOGGLE_EDITOR' };
type action_SET_CATEGORIES = { type: 'SET_CATEGORIES', categories: Array<String> };
type action_EDIT_CATEGORY = { type: 'EDIT_CATEGORY', idx: number, name: String };
type action_LOGIN = { type: 'LOGIN', email: String, password: String };
type action_SET_TOKEN = { type: 'SET_TOKEN', token: ?string };
type action_CLEAR_TIME_INTERVAL = { type: 'CLEAR_TIME_INTERVAL' };
type action_CLEAR_FETCH_INTERVAL = { type: 'CLEAR_FETCH_INTERVAL' };
type action_SET_TIME_INTERVAL = { type: 'SET_TIME_INTERVAL', value: IntervalID };
type action_SET_FETCH_INTERVAL = { type: 'SET_FETCH_INTERVAL', value: IntervalID };
type action_LOGOUT = { type: 'LOGOUT' };
type AppActionType =
    action_FONTS_LOADED
  | action_FETCH_USER
  | action_FETCH_USER_SUCCESS
  | action_API_ERROR
  | action_CLEAR_ERROR
  | action_SET_TIME
  | action_FETCH_LAST_ITEM
  | action_FETCH_LAST_ITEM_SUCCESS
  | action_TOGGLE_SELECTED_CATEGORY
  | action_CLEAR_SELECTED_CATEGORIES
  | action_CHANGE_TASK
  | action_TOGGLE_EDITOR
  | action_SET_CATEGORIES
  | action_EDIT_CATEGORY
  | action_LOGIN
  | action_SET_TOKEN
  | action_CLEAR_TIME_INTERVAL
  | action_CLEAR_FETCH_INTERVAL
  | action_SET_TIME_INTERVAL
  | action_SET_FETCH_INTERVAL
  | action_LOGOUT;
*/

function appReducer(
  state /*: AppStateType */ = {
    currenttime: new Date(),
    lastitem: {
      isIdle: true,
      categories: [],
      time_started: new Date()
    },
    selectedcategories: new Set(),
    user: {},
    usercategories: [],
    userloading: false,
    itemloading: false,
    noitemloaded: true,
    editoropen: false,
    saving: false,
    error: null,
    fontsloaded: false,
    token: null,
    timer_interval: null,
    fetch_interval: null
  },
  action /*: AppActionType */
) {
  switch (action.type)
  {
    case 'FONTS_LOADED':
      return {
        ...state,
        fontsloaded: true
      };

    case 'FETCH_USER':
      tsclient.getUser().then(
        user => store.dispatch({ type: 'FETCH_USER_SUCCESS', user })
      ).catch(
        error => store.dispatch({ type: 'API_ERROR', call: action.type, error })
      );
      return {
        ...state,
        saving: action.saving ? false : state.saving,
        editoropen: action.saving ? false : state.editoropen,
        userloading: true
      };
    
    case 'FETCH_USER_SUCCESS':
      return {
        ...state,
        userloading: false,
        user: action.user,
        usercategories: action.user.categories.sort(
          (a, b) => a.order < b.order ? -1 : 1
        )
      };

    case 'API_ERROR':
      setTimeout(
        () => store.dispatch({ type: 'CLEAR_ERROR' }),
        3000
      );
      if (action.error.status == 401) {
        setImmediate(() => store.dispatch({ type: 'LOGOUT' }));
      }
      const errstr = `${action.call.toString()} : ${action.error.message}`;
      return {
        ...state,
        userloading: false,
        saving: false,
        error: errstr
      };
    
    case 'CLEAR_ERROR':
      return {
        ...state,
        error: null
      };

    case 'SET_TIME':
      return {
        ...state,
        currenttime: action.time
      }

    case 'FETCH_LAST_ITEM':
      tsclient.getLastItem().then(
        lastitem => store.dispatch({ type: 'FETCH_LAST_ITEM_SUCCESS', lastitem })
      ).catch(
        error => store.dispatch({ type: 'API_ERROR', call: action.type, error })
      );
      return {
        ...state,
        itemloading: true
      };
    
    case 'FETCH_LAST_ITEM_SUCCESS':
      const { lastitem } = action;
      lastitem.isIdle = !lastitem.categories.length || (lastitem.categories.length === 1 && lastitem.categories[0] === 'END');
      return {
        ...state,
        noitemloaded: false,
        itemloading: false,
        lastitem
      };

    case 'TOGGLE_SELECTED_CATEGORY':
      const selectedcategories = new Set(state.selectedcategories);
      if (selectedcategories.has(action.category))
      {
        selectedcategories.delete(action.category);
      }
      else
      {
        selectedcategories.add(action.category);
      }
      return {
        ...state,
        selectedcategories
      }
    
    case 'CLEAR_SELECTED_CATEGORIES':
      return {
        ...state,
        selectedcategories: new Set()
      };
    
    case 'CHANGE_TASK':
      tsclient.putTimesheetItem(action.categories).then(
        lastitem => {
          store.dispatch({ type: 'FETCH_LAST_ITEM_SUCCESS', lastitem });
          store.dispatch({ type: 'CLEAR_SELECTED_CATEGORIES' });
        }
      ).catch(
        error => store.dispatch({ type: 'API_ERROR', call: action.type, error })
      );
      return state;
    
    case 'TOGGLE_EDITOR':
      return {
        ...state,
        editoropen: !state.editoropen
      };
    
    case 'SET_CATEGORIES':
      tsclient.putUserCategories(action.categories).then(
        () => store.dispatch({ type: 'FETCH_USER', saving: true })
      ).catch(
        error => store.dispatch({ type: 'API_ERROR', call: action.type, error })
      );
      return {
        ...state,
        saving: true
      };

    case 'EDIT_CATEGORY':
      const usercategories = Array.from(state.usercategories);
      let cat = usercategories[action.idx];
      if (!cat)
      {
        cat = { category: { name: action.name } };
      }
      else
      {
        cat.category.name = action.name;
      }
      usercategories[action.idx] = cat;
      return {
        ...state,
        usercategories
      }
    
    case 'SET_TOKEN':
      tsclient.setToken(action.token).then(
        token => {
          if (token) {
            function setTime() {
              store.dispatch({ type: 'SET_TIME', time: new Date() });
            }
            function fetchLast() {
              store.dispatch({ type: 'FETCH_LAST_ITEM' });
            }
            function fetchUser() {
              store.dispatch({ type: 'FETCH_USER' });
            }
            
            const tint = setInterval(setTime, 5000);
            const fint = setInterval(fetchLast, 30000);
            store.dispatch({ type: 'SET_TIME_INTERVAL', value: tint });
            store.dispatch({ type: 'SET_FETCH_INTERVAL', value: fint });
            
            fetchUser();
            fetchLast();
            setTime();
          } else {
            store.dispatch({ type: 'CLEAR_TIME_INTERVAL' });
            store.dispatch({ type: 'CLEAR_FETCH_INTERVAL' });
          }
        }
      );
      return {
        ...state,
        token: action.token
      }
  
    case 'LOGIN':
      bluebird.cast(
        tsclient.login(action.email, action.password, true)
      ).tap(
        token => AsyncStorage.setItem('tts:token', token)
      ).then(
        token => store.dispatch({ type: 'SET_TOKEN', token })
      ).catch(
        error => store.dispatch({ type: 'API_ERROR', call: action.type, error })
      );
      return state;
    
    case 'SET_TIME_INTERVAL':
      return {
        ...state,
        timer_interval: action.value
      }
    
    case 'CLEAR_TIME_INTERVAL':
      if (state.timer_interval) {
        clearInterval(state.timer_interval);
      }
      return {
        ...state,
        timer_interval: null
      }
  
    case 'SET_FETCH_INTERVAL':
      return {
        ...state,
        fetch_interval: action.value
      }
    
    case 'CLEAR_FETCH_INTERVAL':
      if (state.fetch_interval) {
        clearInterval(state.fetch_interval);
      }
      return {
        ...state,
        fetch_interval: null
      }

    case 'LOGOUT':
      AsyncStorage.removeItem('tts:token').then(
        () => tsclient.logout()
      ).then(
        () => store.dispatch({ type: 'SET_TOKEN', token: null })
      );
      return state;

    default:
      return state;
  }
}

const middleware = applyMiddleware();
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(
  appReducer,
  composeEnhancers(
    middleware,
    __DEV__ ? Reactotron.createEnhancer() : undefined
  )
);

export default store;